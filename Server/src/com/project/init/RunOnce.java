package com.project.init;

import com.project.global.dao.*;
import com.project.global.pojos.*;
import com.project.global.service.*;

import net.sf.json.JSONArray;

import java.lang.reflect.Field;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.core.utils.*;


@SuppressWarnings("unused")
public class RunOnce extends Thread implements GlobalUtils  {
	
	protected static void WorkEvent(){
		BaseDao baseDao = new BaseDao();

		Role role;
		Security sec;
		User user;
		
		role = new Role();
		role.setRolename("绠＄悊鍛�");
		role.setPolicies("[{\"Location\":\"*\",\"Access\":\"*\"}]");
		//baseDao.save(role);
		
		sec = new Security();
		sec.setPassword("123456");
		sec.setAccountState(0);
		sec.setHost("0.0.0.0/0");
		//baseDao.save(sec);
		
		user = new User();
		user.setUID("A2018101");
		user.setUsername("LiTengTeng");
		user.setSex("m");
		user.setDept("鍏氭敮閮�");
		user.setGrade("2016绾�");
		user.setClassSeq("");
		user.setROLE(role);
		user.setSECURITY(sec);
		//baseDao.save(user);

		role = new Role();
		role.setRolename("鏁欏笀");
		role.setPolicies("[{\"Location\":\"class\",\"Access\":\"read\"}]");
		//baseDao.save(role);
		
		sec = new Security();
		sec.setPassword("123456");
		sec.setAccountState(0);
		sec.setHost("0.0.0.0/0");
		//baseDao.save(sec);
		
		user.setUID("T2018001");
		user.setUsername("HAHA");
		user.setSex("m");
		user.setDept("鍏氭敮閮�");
		user.setGrade("2016绾�");
		user.setClassSeq("13");
		user.setROLE(role);
		user.setSECURITY(sec);
		//baseDao.save(user);
		
		role = new Role();
		role.setRolename("瀛︾敓");
		role.setPolicies("[{\"Location\":\"self\",\"Access\":\"*|~audit|\"}]");
		//baseDao.save(role);
		
		sec = new Security();
		sec.setPassword("123456");
		sec.setAccountState(0);
		sec.setHost("0.0.0.0/0");
		//baseDao.save(sec);
		
		user = new User();
		user.setUID("S0180001");
		user.setUsername("HouHouHou");
		user.setSex("m");
		user.setDept("鍏氭敮閮�");
		user.setGrade("2016绾�");
		user.setClassSeq("13");
		user.setROLE(role);
		user.setSECURITY(sec);
		//baseDao.save(user);

		Award r1 = new Award(), r2 = new Award();
		r1.setName("鏂楀浘澶ц禌");
		r1.setField("瑁呴�肩被");
		r1.setTeam("鐝骇浣�");
		r1.setCollege("涓汉椤�");
		r1.setLevel("鐗圭瓑濂�");
		r1.setTutor("鏉庤吘鑵�");
		r1.setDetail("鑾峰浣滃搧:闈欓潤瑁呴�肩収");
		r1.setWhen(new Date());
		r1.setTemplate(1);
		//baseDao.save(r1);
		
		r2.setTutor("娓ｆ福鐭�");
		r2.setTemplate(0);
		//baseDao.save(r2);
		
		R_User_Award R = new R_User_Award();
		UserAward RR = new UserAward();
		R.setUser(user);
		R.setAward(r2);
		RR.setRegisterId(R);
		RR.setAuditState(0);
		//baseDao.save(RR);
	}
	
	public static void main(String[] args) {
		int mask = 24;
		System.out.println((0xFFFFFFFFL << (32 - mask)));
		System.out.println(~(int)(0xFFFFFFFFL >>> mask));
		//new Thread().run();
		//currentThread().suspend();
	}
	
	public void run(){
		WorkEvent();
		ConvertToIdleThread();
	}
	
	protected void ConvertToIdleThread(){
		while(true){
			try{
				sleep(15 * 60 * 1000);
			} catch(Exception e) {
				
			}
		}
	}

}
