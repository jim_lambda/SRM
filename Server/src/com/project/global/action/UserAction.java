package com.project.global.action;

import java.util.ArrayList;

import com.core.utils.ActionContextUtils;
import com.project.global.service.UserService;

@SuppressWarnings("serial")
public class UserAction extends BaseAction {

	private String userid;
    private String password;
    private UserService service = new UserService();
    private static final int Interval = 300; //milliseconds
    
    
    public String login(){
    	loginStatus status;

    	if(System.currentTimeMillis() - UserService.currentSession().getLastAccessedTime() < Interval &&
    															!UserService.currentSession().isNew()){
    		setResultCollcation(loginStatus.SHORT_INTERVAL);
    		return	jsonStatus.FINISHED.toString();
    	}
    	
    	status = service.login(userid, password);
    	
    	setResultCollcation(status);
    	
    	return	jsonStatus.FINISHED.toString();
    }
    
    public String logout(){
    	service.logout(UserService.currentSession());
    	
    	if(!ActionContextUtils.isAjaxRequest()){
    		return	jsonStatus.LOGIN.toString();
    	}
    	
    	setResultCollcation(jsonStatus.FINISHED);
    	
        return	jsonStatus.FINISHED.toString();
    }
    
    public String showUser() {
    	return jsonStatus.FINISHED.toString();
    }
    
    public String findUser() {
    	dbStatus status = dbStatus.INVAILD_PARAMS;
    	ArrayList<Object> users= new ArrayList<Object>();
    	
    	if(userid.equals("grade")) {
    		status = service.findUserByGrade(users);
    	}else if(userid.equals("class")) {
    		status = service.findUserByClass(users);
    	}else {
    		Object user = service.findUserByUID(userid);
    		users.add(user);
    		status = (user != null) ? dbStatus.SUCCESS : dbStatus.NO_RECORD;
    	}
    	
    	if(!isSuccess(status)) {
        	setResultCollcation(status);
        	return	jsonStatus.FINISHED.toString();
    	}

    	setResultCollcation(users, status);
    	
    	return jsonStatus.FINISHED.toString();
    }
	
    // ///////////////////////////////////////////
    // //getter/setter方法
    // //////////////////////////////////////////
	
    @Override
	public void validate() 
    {
		//DataVerification
		
		if(userid == null || !userid.matches("^\\w{1,10}$")){
			userid = "";
		}

		if(password == null){
			password = "";
		}

    }
    
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
   
}
