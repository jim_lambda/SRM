package com.project.global.action;

import com.opensymphony.xwork2.ModelDriven;
import com.project.global.pojos.Award;
import com.project.global.service.AwardService;
import com.project.global.service.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class AwardAction extends BaseAction implements ModelDriven<Award>{

	private Award award = new Award();
	private AwardService service = new AwardService();
	private UserAwardAction userAwardAction = new UserAwardAction();
	
	
	public String show(){
		dbStatus status;
		List<Object> awards = new ArrayList<Object>();
		
		status = service.showAwardsByName(award.getName(), awards);
		
		if(!isSuccess(status)){
			setResultCollcation(status);
			return	jsonStatus.FINISHED.toString();
		}
		
		setResultCollcation(awards, status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String add(){
		dbStatus status;
		
		status = service.addAward(award);
		
		if(!isSuccess(status)) {
			setResultCollcation(status);
			return	jsonStatus.FINISHED.toString();
		}


		userAwardAction.setModel(UserService.currentUser(), award, 0);
		
		userAwardAction.ref();
		
		status = dbStatus.lookup((int)userAwardAction.getResultCollcation().get("code"));
		
		if(!isSuccess(status)){
			setResultCollcation(status);
			return jsonStatus.FINISHED.toString();
		}
		
		setResultCollcation(award, status);
		
		return jsonStatus.FINISHED.toString();
	}
	
	public String edit(){
		dbStatus status;
		
		status = service.updateAward(award);
		
		setResultCollcation(status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String del(){
		dbStatus status;
		
		userAwardAction.setModel(UserService.currentUser(), award, 0);
		
		userAwardAction.deref();
		
		status = dbStatus.lookup((int)userAwardAction.getResultCollcation().get("code"));
		
		if(!isSuccess(status)){
			setResultCollcation(status);
			return	jsonStatus.FINISHED.toString();
		}
		
		
		status = service.deleteAward(award);
		
		setResultCollcation(status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String upload(){
		dbStatus status;
		Map<String, Object> header = new HashMap<String, Object>();
		
		status = service.getUploadHeader(header);
		
		if(!isSuccess(status)){
			setResultCollcation(status);
			return	jsonStatus.FINISHED.toString();
		}
		
		setResultCollcation(header, status);
		
		return jsonStatus.FINISHED.toString();
	}
	
    // ///////////////////////////////////////////
    // //getter/setter方法
    // //////////////////////////////////////////
	
	@Override
	public void validate(){
		// DataVerification
		
		award.AwardDescriptionFilter();
	}
	
	@Override
	public Award getModel() {
		return award;
	}
	
}
