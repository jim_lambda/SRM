package com.project.global.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ModelDriven;
import com.project.global.pojos.Award;
import com.project.global.pojos.R_User_Award;
import com.project.global.pojos.User;
import com.project.global.pojos.UserAward;
import com.project.global.service.UserAwardService;

@SuppressWarnings("serial")
public class UserAwardAction extends BaseAction implements ModelDriven<UserAward>{

	private User user = null;
	private Award award = null;
	private UserAward userAward = new UserAward();
	private UserAwardService service = new UserAwardService();
	
	
	public String show(){
		dbStatus status;
		List<Object> awards = new ArrayList<Object>();
		
		status = service.showAwardRelationsByUser(user.getUID(), awards);
		
		if(!isSuccess(status)){
			setResultCollcation(status);
			return	jsonStatus.FINISHED.toString();
		}
		
		setResultCollcation(awards, status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String ref(){
		dbStatus status;
		
		status = service.addAwardRelation(userAward);
		
		setResultCollcation(status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String deref(){
		dbStatus status;
		
		status = service.deleteAwardRelation(userAward);
		
		setResultCollcation(status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
	public String audit()
	{
		dbStatus status;
		
		status = service.updateAwardRelation(userAward);
		
		setResultCollcation(status);
		
		return	jsonStatus.FINISHED.toString();
	}
	
    // ///////////////////////////////////////////
    // //getter/setter方法
    // //////////////////////////////////////////
	
	@Override
	public void validate()
	{
		// DataVerification
		
		R_User_Award R = userAward.getRegisterId();
		
		if(userAward.getRegisterId() == null){
			R = new R_User_Award();
			userAward.setRegisterId(R);
		}
		
		user = R.getUser();
		if(user == null){
			user = new User();
			R.setUser(user);
		}
		if(user.getUID() == null || !user.getUID().matches("^\\w{1,10}$")){
			user.setUID("");
		}
		
		award = R.getAward();
		if(award  == null){
			award = new Award();
			R.setAward(award);
		}
		if(award.getRNO() == null){
			award.setRNO(0);
		}
		
		if(userAward.getAuditState() == null){
			userAward.setAuditState(0);
		}

	}
	
	@Override
	public UserAward getModel() 
	{
		return userAward;
	}
	
	public void setModel(User user, Award award, Integer state) 
	{
		UserAward userAward = new UserAward();
		R_User_Award r = new R_User_Award();
		
		r.setUser(user);
		r.setAward(award);
		userAward.setRegisterId(r);
		userAward.setAuditState(state);
		
		this.userAward = userAward;
	}
	
}
