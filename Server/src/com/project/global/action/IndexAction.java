package com.project.global.action;

import com.core.utils.ActionContextUtils;
import com.project.global.service.UserService;

@SuppressWarnings("serial")
public class IndexAction extends BaseAction {

    public String index() {
		jsonStatus status = jsonStatus.LOGIN;

    	if(UserService.isOnline() == true){
    		status = jsonStatus.INDEX;
    	}
        
    	if(!ActionContextUtils.isAjaxRequest()){
    		return	status.toString();
    	}
    	
    	setResultCollcation(status);
        
        return jsonStatus.FINISHED.toString();
    }
	
}
