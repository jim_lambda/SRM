package com.project.global.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;
import com.core.utils.GlobalUtils;

@SuppressWarnings({"serial", "unchecked"})
public class BaseAction extends ActionSupport implements GlobalUtils {
	
	private Map<String, Object> ResultCollcation = null;
	
	protected void setResultCollcation(Object result, Status status) {
		Map<String, Object> map, msg;
		List<Object> data;
		
		map = new HashMap<String, Object>();
		msg = GlobalUtils.serializeToMap(status);
		
		if(result instanceof List) {
			data = (List<Object>)result;
		}else {
			data = new ArrayList<Object>();
			data.add(result);
		}
		
		map.put("code",  msg.get("code"));
		map.put("msg",   msg.get("msg"));
		map.put("count", data.size());
		map.put("data",  data);
		
		this.ResultCollcation = map;
	}
	
	protected void setResultCollcation(Status Status) {
		this.ResultCollcation = GlobalUtils.serializeToMap(Status);
	}
	
    // ///////////////////////////////////////////
    // //getter/setter方法
    // //////////////////////////////////////////
	
	public Map<String, Object> getResultCollcation() {
		return ResultCollcation;
	}

	protected void setResultCollcation(Map<String, Object> JSON) {
		this.ResultCollcation = JSON;
	}
	
}
