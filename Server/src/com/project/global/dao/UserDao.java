package com.project.global.dao;

import org.hibernate.query.Query;

import java.util.List;

import com.project.global.pojos.User;

@SuppressWarnings({ "unchecked" })
public class UserDao extends BaseDao {
	
	public User findUserByUID(String userId) {
		return ((ComplexQuery<User>)(session)->{
			return session.get(User.class, userId);
		}).find(false);
	}
	//
	public List<User> findUserByClass(String Class) {
		return ((SimpleQuery<List<User>>)(list)->{
			return findUserByClass(Class, list, false, 0, 50);
		}).put();
	}
		
	public List<User> findUserByClass(String Class, List<User> users, boolean persist, int page, int range) {
		return ((ComplexQuery<List<User>>)(session)->{
			Query<User> query = session.createQuery("from User u where u.ClassSeq = :class");
			query.setParameter("class", Class);
			
			users.addAll(query.getResultList());
			return users;
		}).find(persist);
	}
	//
	public List<User> findUserByGrade(String grade) {
		return ((SimpleQuery<List<User>>)(list)->{
			return findUserByGrade(grade, list, false, 0, 50);
		}).put();
	}
	
	public List<User> findUserByGrade(String grade, List<User> users, boolean persist, int page, int range) {
		return ((ComplexQuery<List<User>>)(session)->{
			Query<User> query = session.createQuery("from User u where u.Grade = :grade");
			query.setParameter("grade", grade);
			
			users.addAll(query.getResultList());
			return users;
		}).find(persist);
	}

}
