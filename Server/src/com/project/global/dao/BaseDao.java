package com.project.global.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Transaction;
import org.hibernate.Session;

import com.core.utils.GlobalUtils.dbStatus;
import com.core.utils.HibernateUtils;

@SuppressWarnings("rawtypes")
public class BaseDao {
	protected Transaction g_trans = null;

	protected dbStatus save(Object pojo, boolean persist) {
		return ((BeginTrans)(session)->{
			session.saveOrUpdate(pojo);
			return dbStatus.SUCCESS;
		}).exec(this, persist);
	}

	protected dbStatus delete(Object pojo, boolean persist) {
    	return ((BeginTrans)(session)->{
    		session.delete(pojo);
    		return dbStatus.SUCCESS;
    	}).exec(this, persist);
	}
	
	protected static void rollback(Transaction trans) {
		if(trans != null){
			trans.rollback();
		}
		HibernateUtils.closeSession();
	}
	
	protected static void closeSession(Transaction trans) {
		if(trans != null){
			trans.commit();
		}
		HibernateUtils.closeSession();
	}
	
	
	protected interface SimpleQuery<T> {
		public T Callback(List list);
		
		default T put(){
	    	List<?> list = new ArrayList<Object>();
	    	T result = this.Callback(list);

	    	return result;
	    }
	}
	
	protected interface ComplexQuery<T> {
	    public T Callback(Session session);
	    
		default T find(boolean persist){
			Session session;
			
			session = HibernateUtils.getSession();
			
			T result = this.Callback(session);
			
			if(!persist){
				closeSession(null);
			}
			
			return result;
	    }
	}

	protected interface BeginTrans {
	    public dbStatus Callback(Session session);
	    
		default dbStatus exec(BaseDao context, boolean persist){
			Session session;
			Transaction trans = context.g_trans;
			
			session = HibernateUtils.getSession();
			if(trans == null){
				trans = session.beginTransaction();
			}
			
			dbStatus status = null, 
					result = this.Callback(session);
			
			try {
				result = this.Callback(session);
				
				if(!persist){
					closeSession(trans);
					trans = null;
				}
			} catch(Exception ex){
				trans.rollback();
				
				status = dbStatus.lookup(HibernateUtils.getSQLErrorCode(ex));
				if(status == null){
					status = dbStatus.INVAILD_PARAMS;
				}
				
				result = status;
			}
			
			if(!persist && trans != null){
				closeSession(null);
				trans = null;
			}
			
			context.g_trans = trans;
			
			return result;
	    }
	}
	
}
