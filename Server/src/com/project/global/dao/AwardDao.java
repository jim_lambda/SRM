package com.project.global.dao;

import java.util.List;

import org.hibernate.query.Query;

import com.core.utils.GlobalUtils.dbStatus;
import com.project.global.pojos.R_User_Award;
import com.project.global.pojos.Award;
import com.project.global.pojos.User;
import com.project.global.pojos.UserAward;

@SuppressWarnings({"unchecked"})
public class AwardDao extends BaseDao {
	/*	select * From ??? Where xxx >=(
			select xxx From ??? Order By id limit start,1
			) limit size;
			*/
	public Award findAwardByUniqueId(int awardNO) {
		return ((ComplexQuery<Award>)(session)->{
			return session.get(Award.class, awardNO);
		}).find(false);
	}
	//
	public List<Award> findAwardByName(String name) {
		return ((SimpleQuery<List<Award>>)(list)->{
			return findAwardByName(name, list, false, 0, 50);
		}).put();
	}
	
	public List<Award> findAwardByName(String name, List<Award> awards, boolean persist, int page, int range) {
		//Combine
		return ((ComplexQuery<List<Award>>)(session)->{
			assert(awards != null);
			Query<Award> query = session.createQuery("from Award r where r.Name like :name and r.Template > 0");
			query.setParameter("name", "%" + name + "%");

			awards.addAll(query.getResultList());
			return awards;
		}).find(persist);
	}
	//
	public List<Award> findAwardByUserId(String uid) {
		return ((SimpleQuery<List<Award>>)(list)->{
			return findAwardByUserId(uid, list, false, 0, 50);
		}).put();
	}
	
	public List<Award> findAwardByUserId(String uid, List<Award> awards, boolean persist, int page, int range) {
		//Combine
		return ((ComplexQuery<List<Award>>)(session)->{
			Query<Award> query = session.createQuery("from Award r where r.RNO in (" +
													"select R.RegisterId.award.RNO from UserAward R where R.RegisterId.user.UID = :uid)");
			query.setParameter("uid", uid);

			awards.addAll(query.getResultList());
			return awards;
		}).find(persist);
	}
	//
	public List<UserAward> findRelationByUserId(String uid) {
		return ((SimpleQuery<List<UserAward>>)(list)->{
			return findRelationByUserId(uid, list, false, 0, 50);
		}).put();
	}
	
	public List<UserAward> findRelationByUserId(String uid, List<UserAward> relations, boolean persist, int start, int range) {
		return ((ComplexQuery<List<UserAward>>)(session)->{
			Query<UserAward> query = session.createQuery("from UserAward R where R.RegisterId.user.UID = :uid");
			query.setParameter("uid", uid);
			
			relations.addAll(query.getResultList());
			return relations;
		}).find(persist);
	}

	public UserAward findRelationByUniqueId(User u, Award r) {
		return ((ComplexQuery<UserAward>)(session)->{
			R_User_Award relation = new R_User_Award();
				
			relation.setUser(u);
			relation.setAward(r);
			
			return session.get(UserAward.class, relation);
		}).find(false);
	}
	
	public Long CountRelationByOthers(User u, Award r) {
		return ((ComplexQuery<Long>)(session)->{
			Query<Long> query = session.createQuery("select count(1) from UserAward R where R.RegisterId.award.RNO = :rno and R.RegisterId.user.UID <> :uid");
			query.setParameter("rno", r.getRNO());
			query.setParameter("uid", u.getUID());
			
			return query.uniqueResult();
		}).find(false);
	}
	
	public dbStatus updateAwardDescription(Award r, boolean persist) {
		return this.save(r, persist);
	}
	
	public dbStatus updateAwardBinding(UserAward b, boolean persist) {
		return this.save(b, persist);
	}
	
	public dbStatus deleteAward(Award r, boolean persist) {
		return this.delete(r, persist);
	}
	
	public dbStatus deleteAwardBinding(UserAward b, boolean persist) {
		return this.delete(b, persist);
	}

}
