package com.project.global.pojos;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserAward implements Serializable {

	R_User_Award	RegisterId;
	Integer			AuditState;
	
	public R_User_Award getRegisterId() {
		return RegisterId;
	}
	public void setRegisterId(R_User_Award registerId) {
		RegisterId = registerId;
	}
	public Integer getAuditState() {
		return AuditState;
	}
	public void setAuditState(Integer auditState) {
		AuditState = auditState;
	}

}
