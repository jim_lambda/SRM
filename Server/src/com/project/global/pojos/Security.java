package com.project.global.pojos;

import java.io.Serializable;

import com.core.utils.GlobalUtils;
import com.core.utils.GlobalUtils.loginStatus;
import com.core.utils.HmacSha1;
import com.core.utils.NetworkUtils;

@SuppressWarnings("serial")
public class Security implements Serializable {

	String	SID;
	String	Password;
	Integer	AccountState;
	String	Host;
	
	
	public String getSID() {
		return SID;
	}
	public void setSID(String sID) {
		SID = sID;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		String pass2;
		if(SID == null || SID.isEmpty()){
			pass2 = HmacSha1.HashPwd(password);
		}else{
			pass2 = password;
		}
		Password = pass2;
	}
	public Integer getAccountState() {
		return AccountState;
	}
	public void setAccountState(Integer accountState) {
		AccountState = accountState;
	}
	public String getHost() {
		return Host;
	}
	public void setHost(String host) {
		Host = host;
	}

	public loginStatus SerurityMatch(Security sec){

		if (!this.getPassword().equals(sec.getPassword())) {
			return	loginStatus.INVAILD_PWD;
		}

		if (!GlobalUtils.isSuccess(this.getAccountState())) {
			return	loginStatus.USER_FROZEN;
		}

		if (!isAccessPointAllowed(sec.getHost())){
			return	loginStatus.UNEXPECTED_IP;
		}

		return loginStatus.SUCCESS;
	}

    private boolean	isAccessPointAllowed(String AccessPoint){
    	String segs[];
    	int remoteIP, allowedIP, mask, compareResult;
    	
    	
    	segs = this.getHost().split("/", 2);
    	
    	mask = Integer.parseInt(segs[1]);
    	allowedIP  = NetworkUtils.getInternalAddress(segs[0]);
    	remoteIP = NetworkUtils.getInternalAddress(AccessPoint);
    	
    	compareResult = ((allowedIP ^ remoteIP) & ~(int)(0x0FFFFFFFFL >>> mask));
    	
    	return (compareResult == 0);
    }
}
