package com.project.global.pojos;

import java.io.Serializable;
import org.apache.struts2.json.annotations.JSON;

@SuppressWarnings("serial")
public class R_User_Award implements Serializable {

	User	user;
	Award	award;
	
	@JSON(serialize = false)
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Award getAward() {
		return award;
	}
	public void setAward(Award award) {
		this.award = award;
	}
	
}
