package com.project.global.pojos;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;

import com.core.utils.StringUtils;

@SuppressWarnings("serial")
public class Award implements Serializable {
	
	Integer	RNO;		//序号
	String	Name; 		//奖项名
	String	Field;		//参赛类型
	String	College;	//承办学院
	String	Team;		//参赛团队
	String	Level;		//获奖等级
	String	Tutor;		//指导老师
	String	Detail;		//概要
	Date	When;		//获奖时间
	String  Prove;		//证明材料
	Integer	Template;	//引用其它奖项
	
	public Integer getRNO() {
		return RNO;
	}
	public void setRNO(Integer rNO) {
		RNO = rNO;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getField() {
		return Field;
	}
	public void setField(String field) {
		Field = field;
	}
	public String getCollege() {
		return College;
	}
	public void setCollege(String college) {
		College = college;
	}
	public String getTeam() {
		return Team;
	}
	public void setTeam(String team) {
		Team = team;
	}
	public String getLevel() {
		return Level;
	}
	public void setLevel(String level) {
		Level = level;
	}
	public String getTutor() {
		return Tutor;
	}
	public void setTutor(String tutor) {
		Tutor = tutor;
	}
	public String getDetail() {
		return Detail;
	}
	public void setDetail(String detail) {
		Detail = detail;
	}
	public Date getWhen() {
		return When;
	}
	public void setWhen(Date when) {
		When = when;
	}
	public String getProve() {
		return Prove;
	}
	public void setProve(String prove) {
		Prove = prove;
	}
	public Integer getTemplate() {
		return Template;
	}
	public void setTemplate(Integer template) {
		Template = template;
	}
	
	
	public void AwardDescriptionFilter() 
	{	
		for (Field field : this.getClass().getDeclaredFields())
		{
			String Typename;
			Object Value;
			
			field.setAccessible(true);
		try{
			Typename = field.getType().getSimpleName();
			Value = field.get(this);
			
		    if(Typename.equals("String")){
		        field.set(this, StringUtils.htmlEncode((String)Value, false));
		    }
		}catch(Exception e){}
		}
		
	    if(this.getRNO() == null){
	    	this.setRNO(0);
	    }
	    
	    if(this.getWhen() == null){
	    	this.setWhen(new Date());
	    }
	    
	    if(this.getTemplate() == null){
	    	this.setTemplate(0);
	    }
	}
	
}
