package com.project.global.pojos;

import java.io.Serializable;

import java.util.List;
import java.util.Map;

import com.project.global.pojos.User;
import com.core.utils.GlobalUtils;
import com.core.utils.StringUtils;


@SuppressWarnings("serial")
public class Role implements Serializable {

	String	RID;
	String	Rolename;
	String	Policies;
	
	
    public String getRID() {
		return RID;
	}

	public void setRID(String rID) {
		RID = rID;
	}

	public String getRolename() {
		return Rolename;
	}

	public void setRolename(String rolename) {
		Rolename = rolename;
	}

	public String getPolicies() {
		return Policies;
	}

	public void setPolicies(String policies) {
		Policies = policies;
	}
	
	public static boolean PolicyMatch(User self, User target, String operation){
		List<Map<String, Object>> Policy;
		int	flag = 0;
		
		
		if(self == null || target == null) {
			return false;
		}
		
		Policy = GlobalUtils.unserializeFromJSON(self.getROLE().getPolicies());

		for(Map<String, Object> x: Policy){
			String Location = (String) x.get("Location");
			String Access   = (String) x.get("Access");
			
			switch(Location) {
			case "*":
					break;
			case "self":
				if(self.getUID().equals(target.getUID())){
					break;
				}
				continue;
			case "grade":
				if(self.getGrade().equals(target.getGrade())){
					break;
				}
				continue;
			case "class":
				if(self.getClassSeq().equals(target.getClassSeq())){
					break;
				}
				continue;
			default:
				continue;
			}

			{
				if(StringUtils.match(String.format("(%s\\|)|(\\*\\|)", operation), Access)){
					flag |= 0x1;
				}
				
				if(StringUtils.match(String.format("(~%s\\|)", operation), Access)){
					flag &= ~0x1;
					break;
				}
			}
			
		}


		if((flag & 0x1) == 0x1){
			return true;
		}
		
		return false;
	}

}
