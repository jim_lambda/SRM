package com.project.global.pojos;

import java.io.Serializable;
import org.apache.struts2.json.annotations.JSON;

@SuppressWarnings("serial")
public class User implements Serializable {
	
	String		UID;
	String		Username;
	String		Sex;
	String		Dept;
	String		Grade;
	String		ClassSeq;
	Role		ROLE;
	Security	SECURITY;
	
	
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getSex() {
		return Sex;
	}
	public void setSex(String sex) {
		Sex = sex;
	}
	public String getDept() {
		return Dept;
	}
	public void setDept(String dept) {
		Dept = dept;
	}
	public String getGrade() {
		return Grade;
	}
	public void setGrade(String grade) {
		Grade = grade;
	}
	public String getClassSeq() {
		return ClassSeq;
	}
	public void setClassSeq(String classSeq) {
		ClassSeq = classSeq;
	}
	@JSON(serialize = false)
	public Role getROLE() {
		return ROLE;
	}
	public void setROLE(Role rOLE) {
		ROLE = rOLE;
	}
	@JSON(serialize = false)
	public Security getSECURITY() {
		return SECURITY;
	}
	public void setSECURITY(Security sECURITY) {
		SECURITY = sECURITY;
	}

}
