package com.project.global.service;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

import com.core.utils.GlobalUtils;
import com.core.utils.HmacSha1;
import com.project.global.dao.AwardDao;
import com.project.global.pojos.User;
import com.project.global.pojos.Award;
import com.project.global.pojos.Role;


public class AwardService implements GlobalUtils {

	private UserAwardService	userAwardService = new UserAwardService();
	private AwardDao			dao = new AwardDao();
	private User 				s_user = UserService.currentUser(); 
	
	
	public dbStatus showAwardsByName(String awardName, List<Object> result)
	{
		if(!Role.PolicyMatch(s_user, s_user, "list")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Everyone
		
		List<Award> awards = dao.findAwardByName(awardName);
		
		if(awards.isEmpty()){
			return dbStatus.NO_RECORD;
		}
		
		result.addAll(awards);
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus addAward(Award award)
	{	
		if(!Role.PolicyMatch(s_user, s_user, "add")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Student, Administrator
		
		award.setRNO(null);
		
		dbStatus status = dao.updateAwardDescription(award, false);
		
		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus updateAward(Award award)
	{
		if(!Role.PolicyMatch(s_user, s_user, "update")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Student
		
		// whether is Owner.
		// Lock when was audited.
		
		if(!userAwardService.isAwardOnlyOwner(s_user, award)){
			return dbStatus.ACCESS_DENIED;
		}
		
		if(userAwardService.isAwardLocked(award)){
			return dbStatus.RECORD_LOCKED;
		}
		
		
		dbStatus status = dao.updateAwardDescription(award, false);
		
		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus deleteAward(Award award) {
		if(!Role.PolicyMatch(s_user, s_user, "read")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Student
		
		// Award could only be deleted by owner. 
		
		if(!userAwardService.isAwardOnlyOwner(s_user, award)) {
			return	dbStatus.BEING_REFERENCED;
		}
	
		
		dbStatus status = dao.deleteAward(award, false);

		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus getUploadHeader(Map<String, Object> formData) {
		if(!Role.PolicyMatch(s_user, s_user, "upload")){
			return dbStatus.POLICY_DENIED;
		}
		
		String base64policy = "";
		String signature = "";
		String Object = UUID.randomUUID().toString();
		String policy = "{"										+
							"\"expiration\":\"%s\"," 				+
							"\"conditions\":[" 						+
								"[\"eq\",\"$key\",\"upload/%s\"],"+
								"[\"content-length-range\",%d,%d]"	+
							"]"										+
						"}";
		
		policy = String.format(policy, Instant.ofEpochMilli(System.currentTimeMillis() + 5 * 60 * 1000)
									, Object
									, 0
									, 10 * 1024 * 1024
									);

		try {
			base64policy = Base64.encodeBase64String(policy.getBytes("UTF-8"));
			signature = Base64.encodeBase64String(HmacSha1.SignUrl(base64policy));
		} catch (Exception e) {
		}
		
		formData.put("key", 			"upload/".concat(Object));
		formData.put("OSSAccessKeyId",  "LTAItkcIC0ZKuD8V");
		formData.put("policy", 		 	base64policy);
		formData.put("Signature", 		signature);
		
		return dbStatus.SUCCESS;
	}
	
}
