package com.project.global.service;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import java.util.List;

import com.core.utils.ActionContextUtils;
import com.core.utils.GlobalUtils;
import com.core.utils.NetworkUtils;
import com.project.global.dao.UserDao;
import com.project.global.pojos.Role;
import com.project.global.pojos.Security;
import com.project.global.pojos.User;


public class UserService implements GlobalUtils {
	
	class SessionStore implements HttpSessionBindingListener
	{
		protected User user;
		
		public SessionStore(User user){
			this.user = user;
		}

		@Override
		public void valueBound(HttpSessionBindingEvent e) {
			SESSIONS.put(user.getUID(), e.getSession());
		}

		@Override
		public void valueUnbound(HttpSessionBindingEvent e) {
			SESSIONS.remove(user.getUID());
		}
		
	} 

    private static final ObjectBuffering<String, HttpSession> SESSIONS = new ObjectBuffering<String, HttpSession>();
    private static final String LOGIN_USER = "AssociatedUser";
    private UserDao	dao = new UserDao();
	private User s_user = currentUser();

	
	public loginStatus login(String userName, String password) {
		loginStatus status;
		
        logout(currentSession());
        
 		if (userName.isEmpty()) {
 			return	loginStatus.INVAILD_USER;
 		} 
 		
 		User user = dao.findUserByUID(userName.trim());

 		if (user == null) {
 			return	loginStatus.INVAILD_USER;
 		}
 		
 		Security sec;

 		sec = new Security();
 		sec.setPassword(password);
 		sec.setHost(NetworkUtils.getIp());
 		
 		status = user.getSECURITY().SerurityMatch(sec);
 				
 		if (!isSuccess(status)) {
 			return	status;
 		}
 			
 		HttpSession loginedSession = getSessionByUserId(user.getUID());
 		
 		if (loginedSession != null){
 			logout(loginedSession);
 		}
 		
        SessionStore newSession = new SessionStore(user);
        currentSession().setAttribute(LOGIN_USER, newSession);
        
        return loginStatus.SUCCESS;
    }

    public void logout(HttpSession session) {
        if (session == null){
        	return;
        }

        session.setAttribute(LOGIN_USER, null);
    }
    
    public User findUserByUID(String userId){
    	if(s_user != null && s_user.getUID().equals(userId)){
    		return s_user;
    	}
    	
    	User r_user = dao.findUserByUID(userId);
    	
		if(!Role.PolicyMatch(s_user, r_user, "get")){
			return null;
		}

    	return r_user;
    }

    public dbStatus findUserByGrade(List<Object> result) {		
    	User s_grade = null;
    	
    	if(s_user != null) {
    		s_grade = new User();
    		s_grade.setGrade(s_user.getGrade());
    	}
    	
		if(!Role.PolicyMatch(s_user, s_grade, "read")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Administrator
		
		List<User> users = dao.findUserByGrade(s_grade.getGrade());
		
		if(users.isEmpty()){
			return dbStatus.NO_RECORD;
		}
		
		result.addAll(users);
		
		return	dbStatus.SUCCESS;
    }
   
    public dbStatus findUserByClass(List<Object> result) {
    	User s_class = null;
    	
    	if(s_user != null) {
    		s_class = new User();
    		s_class.setClassSeq(s_user.getClassSeq());
    	}
    	
		if(!Role.PolicyMatch(s_user, s_class, "read")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Teacher
		
		List<User> users = dao.findUserByClass(s_class.getClassSeq());
		
		if(users.isEmpty()){
			return dbStatus.NO_RECORD;
		}
		
		result.addAll(users);
		
		return	dbStatus.SUCCESS;
    }
    
    
    public static User currentUser() {
        return getLoginedUser(currentSession());
    }

    public static HttpSession currentSession() {
        return ActionContextUtils.getRequest().getSession();
    }
    
    public static boolean isOnline(){
    	return (currentUser() != null);
    }
    
    protected static User getLoginedUser(HttpSession session) {
    	SessionStore loginSession = (SessionStore)session.getAttribute(LOGIN_USER);
    	
        if (loginSession == null){
            return null;
        }

        return loginSession.user;
    }
   
    protected HttpSession getSessionByUserId(String userId) {
        HttpSession	session = SESSIONS.get(userId);
        
        return session;
    }
    
}
