package com.project.global.service;

import java.util.List;

import com.core.utils.GlobalUtils;
import com.project.global.dao.AwardDao;
import com.project.global.pojos.Award;
import com.project.global.pojos.Role;
import com.project.global.pojos.User;
import com.project.global.pojos.UserAward;

public class UserAwardService implements GlobalUtils {

	private UserService	userService = new UserService();
	private AwardDao	dao = new AwardDao();
	private User 		s_user = UserService.currentUser(); 
	
	
	public dbStatus showAwardRelationsByUser(String userId, List<Object> result)
	{
		User r_user = userService.findUserByUID(userId);
		
		if(!Role.PolicyMatch(s_user, r_user, "read")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Everyone
		
		List<UserAward> awards = dao.findRelationByUserId(r_user.getUID());
		
		if(awards.isEmpty()){
			return dbStatus.NO_RECORD;
		}
		
		result.addAll(awards);
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus addAwardRelation(UserAward userAward)
	{
		if(!Role.PolicyMatch(s_user, s_user, "ref")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Student
		
		userAward.getRegisterId().setUser(s_user);
		
		dbStatus status = dao.updateAwardBinding(userAward, false);
		
		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus updateAwardRelation(UserAward userAward)
	{
		User r_user = userService.findUserByUID(userAward.getRegisterId().getUser().getUID());
		
		if(!Role.PolicyMatch(s_user, r_user, "audit")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Administrator
		
		userAward.getRegisterId().setUser(r_user);
		
		dbStatus status = dao.updateAwardBinding(userAward, false);

		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	public dbStatus deleteAwardRelation(UserAward userAward)
	{
		if(!Role.PolicyMatch(s_user, s_user, "deref")){
			return dbStatus.POLICY_DENIED;
		}
		
		// Context at Student
		
		userAward.getRegisterId().setUser(s_user);
		
		dbStatus status = dao.deleteAwardBinding(userAward, false);

		if(!isSuccess(status)) {
			return	status;
		}
		
		return	dbStatus.SUCCESS;
	}
	
	
	protected boolean isAwardLocked(Award award){
		
		UserAward R = dao.findRelationByUniqueId(s_user, award);
		
		if(R != null && R.getAuditState() > 0) {
			return true;
		}
		
		return false;
	}
	
	protected boolean isAwardOnlyOwner(User user, Award award){
		
		return !(dao.CountRelationByOthers(user, award) > 0);
	}
	
}
