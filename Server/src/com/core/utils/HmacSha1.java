package com.core.utils;

import org.apache.commons.codec.digest.*;

/**
 * MD5类。<br>
 * 实现了RSA Data Security,Inc.在提交给IETF的RFC1321中的MD5 message-digest算法。
 */

public class HmacSha1 {
	
	private static HmacUtils MyCrypto = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, "0-65p4./6,8..3EK");
	private static HmacUtils OssCrypto = new HmacUtils(HmacAlgorithms.HMAC_SHA_1, "BtTg6HlnyTHDs9bkA4gcyQnqUCDWYT");
	
	public static String HashPwd(String valueToDigest){

		return MyCrypto.hmacHex(valueToDigest);
	}
	
	public static byte[] SignUrl(String valueToDigest){
		//"AccessKeyId","AccessKeySecret"
		//"LTAItkcIC0ZKuD8V","BtTg6HlnyTHDs9bkA4gcyQnqUCDWYT"

		return OssCrypto.hmac(valueToDigest);
	}
}