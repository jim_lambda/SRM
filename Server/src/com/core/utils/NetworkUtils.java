package com.core.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;

public class NetworkUtils {

	/**
	 * 获取IP地址
	 * @return
	 */
	public static String getIp() {
		HttpServletRequest request = ActionContextUtils.getRequest();
		String ip = request.getHeader("X-real-ip");
		if (StringUtils.isEmpty(ip)) {
			ip = request.getRemoteAddr();
		}
		if(ip.contains(":")) {
			ip = "";
		}

		return ip;
	}
	
	public static int getInternalAddress (String IPText){
		String[]	segs;
		int 		result = 0;
		
		if (IPText == null){
			return 0;
		}
		
		//originIP.replaceFirst("^/", arg1);
		
		if(!IPText.matches("^(\\d{1,3}\\.){3}\\d{1,3}$")){
			return 0;
		}
		
		segs = IPText.split("\\.", 4);
		result = Integer.parseInt(segs[0]) << 24 | Integer.parseInt(segs[1]) << 16 
				| Integer.parseInt(segs[2]) << 8 | Integer.parseInt(segs[3]);

		return result & 0xFFFFFFFF;
	}
	
	
	/**
  InternalAddress     * @param urlStr
     * @param content
     * @return
     */
    public static String fromHttp(String urlStr, String content) {
    	
    	URL url = null;
		HttpURLConnection connection = null;
		try {
			url = new URL(urlStr);
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.connect();  
			DataOutputStream out = new DataOutputStream(connection
					.getOutputStream());
			out.write(content.getBytes("utf-8"));
			out.flush();
			out.close();
			BufferedReader reader =
			new BufferedReader(new InputStreamReader(connection
					.getInputStream(), "utf-8"));
			StringBuffer buffer = new StringBuffer();
			String line = "";
			while ((line = reader.readLine()) != null) {
				buffer.append(line);
			}
			
			reader.close();
			return buffer.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
    	return null;
    }
    
}
