﻿package com.core.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import net.sf.json.JSONArray;

@SuppressWarnings( { "unchecked" })
public interface GlobalUtils {
	
	public static String VERSION = "V2.0 - Beta";
	
	// Return Status
	
	public static interface Status {
        public int getCode();
		public String getMsg();
	}
    
    public static enum jsonStatus implements Status
    {
    	LOGIN	(302, "login"), 
    	INDEX	(302, "index"), 
    	FINISHED(200, "succeed"),
    	FAILED  (500, "error");
    	
    	private final int code; 
    	private final String msg; 
        public int getCode() 		{ return code; }
		public String getMsg() 	{ return msg; }
		private jsonStatus(int code, String msg) { this.code = code; this.msg = msg; } 
		@Override public String toString() { return msg; }
    }
    
    public static enum loginStatus implements Status
    {
    	SUCCESS			( 0, "登陆成功!"),
    	INVAILD_USER	(-1, "用户不存在!"),
    	INVAILD_PWD		(-2, "密码不正确!"),
    	USER_FROZEN		(-3, "该用户已被冻结!"),
    	UNEXPECTED_IP	(-4, "登陆环境异常!"),
    	SHORT_INTERVAL	(-5, "系统繁忙!");
    	
    	private final int code; 
    	private final String msg; 
        public int getCode() 		{ return code; }
		public String getMsg() 	{ return msg; }
		private loginStatus(int code, String msg) { this.code = code; this.msg = msg; } 
    }
    
    public static enum dbStatus implements Status
    {
    	SUCCESS			( 0   , "操作完成!"),
    	POLICY_DENIED	(-1   , "权限不足!"),
    	NO_RECORD		(-2   , "记录为空!"),
    	INVAILD_PARAMS	(-3   , "参数不正确!"),
    	RECORD_LOCKED	(-4   , "该项已锁定!"),
    	ACCESS_DENIED	(-5   , "拒绝访问!"),
    	INVAILD_REFS	(-6   , "该项不适合作为模板!"),
    	DATA_TOO_LONG	(-1406, "字段过长!"),
    	BEING_REFERENCED(-1451, "该项正被他人使用!"),
    	DUPLICATE_RECORD(-1062, "已存在相似的记录!"),
    	INVALID_RECORD  (-1452, "引用的记录不存在!");
    	
    	private final int code; 
    	private final String msg; 
        public int getCode() 		{ return code; }
		public String getMsg() 	{ return msg; }
		private dbStatus(int code, String msg) { this.code = code; this.msg = msg; } 
		public static dbStatus lookup(int code)
		{
			for (dbStatus x : dbStatus.values()) { 
			    if(x.getCode() == code){
			    	return x;
			    }
			} 
		    return null;
		}
    }
    
    
    public static boolean isSuccess(int status) {
    	return (status >= 0);
    }
    
    default boolean isSuccess(Status status) {
    	return (status.getCode() >= 0);
    }
    
    // Buffering Pools
    
    public static class ObjectBuffering<K, V>
    {
    	private ConcurrentHashMap<K, V> Pools;
    	private	int	Size;
    	//private Object lock;
    	
		public ObjectBuffering() { 
			this(-1);
		} 
		
		public ObjectBuffering(int size) { 
			Pools = new ConcurrentHashMap<K, V>(); 
			Size  = size & 0xFFFF;
		} 
		
		public boolean put(K key, V value) { 
			if(Pools.size() >= Size){
				return false;
			}
			Pools.put(key, value);
			return true;
		} 
		
		public boolean remove(K key) { 
			int size = Pools.size();
			Pools.remove(key); 
			return (size > Pools.size());
		} 
		
		public V get(K key) { 
			return Pools.get(key);
		} 
    }
    
    // Object Serialization
	
    public static Map<String, Object> serializeToMap(Object obj) {
    	Map<String, Object> map = new HashMap<String, Object>();   
    
        try{
        	BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());    
	        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();    
	        for (PropertyDescriptor property : propertyDescriptors) {
	        	if (property.getPropertyType().getName().equals("java.lang.Class")) {   
	            	continue;  
	            }  
	                
	            String key = property.getName();
	            Method getter = property.getReadMethod();  
	            Object value = getter!=null ? getter.invoke(obj) : null;  
	            map.put(key, value);  
	        }    
	    } catch (Exception e) { assert(false); }
            
        return map;  
    }    
    
    public static String serializeToJSON(List<Map<String, Object>> list){
		
        return JSONArray.fromObject(list).toString();
    }

    public static List<Map<String, Object>> unserializeFromJSON(String jsonArrayData){
        JSONArray jsonArray = JSONArray.fromObject(jsonArrayData);
        List<Map<String,Object>> mapListJson = (List<Map<String,Object>>)jsonArray;
        
        return mapListJson;  
    }
    
    // Common logs & Debugging tools

}
