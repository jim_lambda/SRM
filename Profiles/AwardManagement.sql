﻿-- MySQL dump 10.13  Distrib 5.6.42, for Win64 (x86_64)
--
-- Host: localhost    Database: AwardManagement
-- ------------------------------------------------------
-- Server version	5.6.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Datadase structure
--

DROP DATABASE IF EXISTS `AwardManagement`;
CREATE DATABASE `AwardManagement` DEFAULT CHARACTER SET utf8;
USE `AwardManagement`

--
-- Datadase User
--

GRANT ALL PRIVILEGES ON `AwardManagement`.* TO `dbm`@`%` identified by 'account_for_testing' WITH GRANT OPTION;
GRANT SELECT,UPDATE,DELETE,INSERT ON `AwardManagement`.* TO `dbm`@`localhost` identified by '0505';

--
-- Table structure for table `award`
--

DROP TABLE IF EXISTS `award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `award` (
  `rno` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `sort` varchar(8) NOT NULL,
  `grade` varchar(16) NOT NULL,
  `dept` varchar(64) NOT NULL,
  `unit` varchar(64) DEFAULT '',
  `tutor` varchar(24) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `date` date NOT NULL,
  `prove` varchar(255) DEFAULT '',
  `template` int(11) DEFAULT 0,
  PRIMARY KEY (`rno`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `award`
--

LOCK TABLES `award` WRITE;
/*!40000 ALTER TABLE `award` DISABLE KEYS */;

/*!40000 ALTER TABLE `award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `rid` varchar(32) NOT NULL,
  `name` varchar(16) NOT NULL,
  `priviliege` varchar(255) NOT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('4028000066ce68ca0166ce68d0860000','管理员','[{\"Location\":\"*\",\"Access\":\"*|\"}]'),('4028000066ce68ca0166ce68d9ac0002','教师','[{\"Location\":\"class\",\"Access\":\"read|get|list|\"}]'),('4028000066ce68ca0166ce68da290004','学生','[{\"Location\":\"self\",\"Access\":\"*|~audit|\"}]');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `security`
--

DROP TABLE IF EXISTS `security`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `security` (
  `sid` varchar(32) NOT NULL,
  `pwd` varchar(40) NOT NULL COMMENT 'HMAC_SHA1(0-65p4./6,8..3EK)',
  `state` int(11) NOT NULL DEFAULT '1',
  `host` varchar(18) NOT NULL DEFAULT '0.0.0.0/0',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `security`
--

LOCK TABLES `security` WRITE;
/*!40000 ALTER TABLE `security` DISABLE KEYS */;
INSERT INTO `security` VALUES ('4028000066ce68ca0166ce68d8630001','a427e8ac0e7d1d9dce2082d0d8759382bc0b117c',0,'0.0.0.0/0');
/*!40000 ALTER TABLE `security` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uid` varchar(8) NOT NULL,
  `name` varchar(16) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `dept` varchar(32) NOT NULL DEFAULT '',
  `grade` varchar(32) NOT NULL DEFAULT '',
  `class` varchar(32)  NOT NULL DEFAULT '',
  `role` varchar(32) DEFAULT NULL,
  `security` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  CONSTRAINT `FK_ROLE` FOREIGN KEY (`role`) REFERENCES `role` (`rid`),
  CONSTRAINT `FK_SECURITY` FOREIGN KEY (`security`) REFERENCES `security` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('A2018101','LiTengTeng','m','党支部','2016级','','4028000066ce68ca0166ce68d0860000','4028000066ce68ca0166ce68d8630001'),('S0180001','HouHouHou','m','党支部','2016级','13','4028000066ce68ca0166ce68da290004','4028000066ce68ca0166ce68d8630001'),('T2018001','HAHA','m','党支部','2016级','13','4028000066ce68ca0166ce68d9ac0002','4028000066ce68ca0166ce68d8630001');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `r_user_award`
--

DROP TABLE IF EXISTS `r_user_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `r_user_award` (
  `uid` varchar(8) NOT NULL,
  `rno` int(11) NOT NULL,
  `state` int(11) DEFAULT 0,
  PRIMARY KEY (`uid`,`rno`),
  CONSTRAINT `FK_R_USER` FOREIGN KEY (`uid`) REFERENCES `user`(`uid`),
  CONSTRAINT `FK_R_REWARD` FOREIGN KEY (`rno`) REFERENCES `award`(`rno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `r_user_award`
--

LOCK TABLES `r_user_award` WRITE;
/*!40000 ALTER TABLE `r_user_award` DISABLE KEYS */;

/*!40000 ALTER TABLE `r_user_award` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-01 20:37:00
