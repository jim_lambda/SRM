﻿layui.config({
   dir:'./'
}).use(['layer', 'form', 'element', 'table', 'jquery', 'upload'], function()
{
  var layer = layui.layer
    ,form = layui.form
    ,element = layui.element
    ,table = layui.table
    ,$=layui.jquery
    ,upload=layui.upload;
    
  function sendRequest(Field)
  {
    var wait = Field.wait;
    var reload = Field.reload;
    var callback = Field.success ? Field.success : ()=>{};
    var scence = (wait || wait === undefined)
                ? layer.load(2, {
                    shade: [0.1,'#000']
                  })
                 : null;
    var d = $.extend(Field, {
      success: function(res){
        if(res.code < 0){
          layer.msg(res.msg, {icon: 5});
          return;
        }
        
        callback(res);

        if(reload){  
          DataCache = res.data;
          
          table.reload("testReload",{
            data: res.data
          });
        } else {
          layer.msg(res.msg, {icon: res.code == 0 ? 6 : 5});
        }
      }
      ,error: function (xhr, textStatus, errorThrown) {
          var errReason = {
                            403:'页面被封锁！',
                            404:'服务器被炸飞！',
                            500:'网络精神异常!'
                           };
          var msgError = errReason[xhr.status];
          
          layer.msg(msgError === undefined ? '未知错误!' : msgError, {icon: 5});
      }
      ,complete: function(event,xhr,options){
        layer.close(scence);
      }
    });
    //d.data = ParamsFilter(d.data, d.url);
    
    $.ajaxCORS(d);
  }
  
  function switchTableCols(name, header)
  {
    table.render({
      elem: '#test'
      ,id: 'testReload'
      ,data: []
      ,title: name
       ,toolbar: '#toolbarTpl'
       ,cellMinWidth: 80
       ,page:true
       ,text:{
         none: '无数据！'
       }
       ,cols: [TableCols[header]]
    });
  }
  
  function ResponseFilter(d, s){
    var v = [];
    
    d.forEach((x)=>{
      cx = x.registerId.award;
      cx.UID = s;
      cx.auditState = x.auditState;
      v.push(cx);
    });
    
    return v;
  }

  function getRole(r){
    var m = r.substring(0,1);
    var name = {'S':'student', 'T':'teacher', 'A':'admin'};
    
    return name[m];
  }
  
  
  var QueryString = $.query()
    ,DataCache = null
    ,CurrentEvent = {context : '', dispatch : null}
    ,AuthId = QueryString['userid']
    ,Role = getRole(AuthId)
    ,View = {
      student:{
        'group'  : AuthId,
        'action' : '#toolTpl'
      }
      ,teacher:{
        'group'  : 'class',
        'action' : null
      }
      ,admin:{
        'group'  : 'grade',
        'action' : '#switchTpl'
      }
    }
    ,TableCols = {
      awardCols:[
         {type:'checkbox',    fixed:'left'}
        ,{field:'name',       title:'竞赛项目'}
        ,{field:'field',       title:'类别'}
        ,{field:'college',    title:'举办学院', sort:true}
        ,{field:'team',       title:'参赛团队'}
        ,{field:'level',       title:'获奖等级'}
        ,{field:'tutor',       title:'指导老师', sort:true}
        ,{field:'detail',     title:'奖项描述'}
        ,{field:'when',       title:'获奖时间', sort:true, templet:(d)=>/\d{4}-\d{1,2}-\d{1,2}/g.exec(d['when'])}
        ,{fixed:'right',       title:'操作',      templet:'#refTpl'}
      ]
      ,studentCols:[
         {type:'checkbox',    fixed:'left'}
        ,{field:'UID',        title:'学号',  sort:true,  event:'show',  style:'cursor:pointer;color:#5e5ec7'}  
        ,{field:'username',    title:'姓名',  edit:'text', sort:true}
        ,{field:'sex',        title:'性别', edit:'text', templet:(d)=>d.sex == 'm' ? '男' : '女'}
        ,{field:'classSeq',    title:'班级', edit:'text'}
        ,{field:'grade',      title:'年级',  edit:'text'}
        ,{field:'dept',        title:'学院', edit:'text'}
        ,{field:'role',        title:'角色', edit:'text', templet:(d)=>getRole(d.UID)}
        ,{fixed:'right',      title:'操作', templet:null}
      ]
      ,studentAwardCols:[
         {type:'checkbox',    fixed:'left'}
        ,{field:'RNO',        title:'序号', sort:true, width: 70}
        ,{field:'name',       title:'竞赛项目',  edit:'text', minWidth: 90}
        ,{field:'field',       title:'类别',      edit:'text', minWidth: 90}
        ,{field:'college',    title:'举办学院',  edit:'text', minWidth: 130}
        ,{field:'team',       title:'参赛团队',  edit:'text', minWidth: 90}
        ,{field:'level',       title:'获奖等级',  edit:'text', minWidth: 90}
        ,{field:'tutor',       title:'指导老师',  edit:'text', minWidth: 90}
        ,{field:'detail',     title:'奖项描述',  edit:'text', minWidth: 90}
        ,{field:'when',       title:'获奖时间',  edit:'text', templet:(d)=>d.when ? /\d{4}-\d{1,2}-\d{1,2}/g.exec(d.when) : '', width: 110}
        ,{field:'prove',       title:'证明材料',  edit:'text', templet:'#proveTpl', minWidth: 200}
        ,{field:'template',    title:'公开',      edit:'text', templet:(d)=>d.template > 0 ? '是' : '否' ,width: 60}
        ,{field:'state',      title:'审核进度',  templet:(d)=>d.auditState > 0 ? "通过" : "未通过", width: 90}
        ,{field:'review',     title:'操作',      templet:View[Role]['action'],  minWidth: 150}
       ]
    };
    

  //刷新奖项信息表格
  $('#awardM').on('click', function(e, awardname)
  {
    switchTableCols('奖项信息表', 'awardCols');

    sendRequest({
          url:    "r/show"
          ,data:  {'name' : awardname ? awardname : '%'}
          ,reload:true
        });
    
    CurrentEvent.context = "awardM";
    CurrentEvent.dispatch = $(this).trigger.bind($(this), 'click', awardname);
  });
     
  //刷新学生信息表格
  $('#studentM').on('click', function()
  {
    switchTableCols('学生信息表', 'studentCols');

    sendRequest({
          url:    "u/findUser"
          ,data:  {'userid' : View[Role]['group']}
          ,reload:true
        });
        
    CurrentEvent.context = "studentM";
    CurrentEvent.dispatch = $(this).trigger.bind($(this), 'click', null);
  });

  //刷新学生关联奖项信息表格
  $('#studentAwardM').on('click', function(e, userid)
  {
    switchTableCols('学生奖项信息表', 'studentAwardCols');
  
    var UID = userid ? userid : AuthId;
  
    sendRequest({
          url:    "d/show"
          ,data:  {'registerId.user.UID' : UID}
          ,reload:true
          ,success:function(result){
            result.data = ResponseFilter(result.data, UID);
          }
        });
          
    CurrentEvent.context = "studentAwardM";
    CurrentEvent.dispatch = $(this).trigger.bind($(this), 'click', userid);
  });
  
  //创建刷新事件
  $('#refresh').on('click', function(){
    CurrentEvent.dispatch();
  });
  
  //创建添加事件
  $('#add').on('click', function(){
    layer.open({
      type:  2,
      title: '信息添加',
      content: './add.html',
      area: ['40%','90%'],
      end: function(){
        
      }
    });
  });
  
  //创建搜索事件
  $('#search').on('click', function(){
    var ctx = CurrentEvent.context
      ,keyword = $('#demoReload').val()
      ,cache = DataCache  //table.cache['testReload']
      ,result = [];
    
    key = {'studentM':'username', 'studentAwardM':'name', 'awardM':'name'}[ctx];
    
    cache.forEach((x)=>{
      if(x[key].match(keyword)){
        result.push(x);
      }
    });
    
    table.reload('testReload', {
      data: result
      ,page: {
        curr: 1
      }
    });
  });
  
  //
  $('#test').on('pushAward', function(e, obj){
      d = $.extend(false, obj.data);
      d.RNO = null;
      d.auditState = null;
      
      var cache = $.extend(true, [], DataCache);
      cache.push(d);
      
      table.reload('testReload', {
        data: cache
      });
  });
  
  //添加奖项
  $('#test').on('addAward', function(e, obj){
    var d = obj.data;
    
    d.RNO = null;
    
    sendRequest({
      url:      "r/add"
      ,data:    d
      ,wait:    false
      ,success: function(result){
        obj.update({'RNO' : result.data[0].RNO});
        DataCache.push(result.data[0]);//
      }
    });
  });
  
  //删除奖项
  $('#test').on('delAward', function(e, obj){
    var d = obj.data;
    
    if(!d.RNO){
      obj.del();
      return;
    }
    
    layer.confirm("该项将被删除?", 
      function(){
        sendRequest({
          url:  "r/del"
          ,data:{'RNO' : d.RNO}
          ,wait:false
          ,success: function (res) {
            for(var i = 0; i < DataCache.length; ++i){//
              if(DataCache[i].RNO == d.RNO){
                DataCache.splice(i, 1);
                break;
              }
            }
            obj.del();
          }
      });
    });
  });
  
  //修改奖项
  $('#test').on('saveAward', function(e, obj){
    var d = obj.data;
    
    if(!d.RNO){
      $('#test').trigger('addAward', obj);
      return;
    }
    
    sendRequest({
      url:  "r/edit"
      ,data:d
      ,wait:false
    });
  });
  
  //用模版添加奖项
  $('#test').on('refAward', function(e, obj){
    var o = $.extend(false, obj);
    var d = $.extend(false, o.data);
    
    d.template = 0;
    o.data = d;
    o.update = ()=>{};
    
    $('#test').trigger('addAward', o);
  });

  //审核奖项
  form.on('switch(auditAward)', function(obj){
    var d = obj.elem;
    var pass = d.checked;
      
    sendRequest({
      url:  "d/audit"
      ,data:{
        'registerId.user.UID'   : d.getAttribute('UID'),
        'registerId.award.RNO'  : d.getAttribute('RNO'),
        'auditState'            : pass ? 1 : 0,
      }
      ,wait:false
    });
  });

  //事件调度
  table.on('tool(test)', function(o){
    var d = o.data;
    
    if(o.event == 'show'){
      $('#studentAwardM').trigger('click', d.UID);
    } else {
      $('#test').trigger(o.event+'Award', [o, $(this)]);
    }
  });

  //多行操作
  table.on('toolbar(test)', function(obj)
  {
    var data = table.checkStatus(obj.config.id).data;

    if(CurrentEvent.context != 'studentAwardM'){
      return;
    }
    
    switch(obj.event){
      case 'deleteAllData':
        layer.confirm("[!] 所有选中项将被删除?", function(){
          var o = { del:()=>{} };
          var confirm = layer.confirm;
          layer.confirm = (s,f)=>f();
          
          data.forEach((x)=>{
            o.data = x;
            $('#test').trigger('delAward', o);
          });
          
          layer.confirm = confirm;
          window.setTimeout($('#refresh').click.bind($('#refresh')), 1000);
        });
        break;
      case 'saveAllData':
      	var o = { update:()=>{} };
      	
      	data.forEach((x)=>{
          o.data = x;
          $('#test').trigger('saveAward', o);
        });
        window.setTimeout($('#refresh').click.bind($('#refresh')), 1000);
        break;
      case 'addData':
      	var o = {};
      	
      	if(data.length == 0){
      		data.push({});
      	}
      	
      	data.forEach((x)=>{
          o.data = x;
          $('#test').trigger('pushAward', o);
        });
        break;
      case 'getCheckLength':
        layer.msg('选中了：'+ data.length + ' 个');
        break;
    };
  });
  
  $('#studentAwardM').click();
});