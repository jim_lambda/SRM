﻿const SERVER_ = 'https://ec2.ifaner.net:9443/';
const OSS_ = 'https://s3.jee.ifaner.net/';

layui.config({
  dir:'./',
  base:'./'
});

layui.use(['jquery'], function()
{
  //layer object
  
  var $=layui.jquery;
     
  //global configuration
  
  $.query = function () 
  {
    var a = {};
    params = decodeURI(location.search);
    params.replace(/(?:^\?|&)([^=&]+)(?:\=)([^=&]+)(?=&|$)/g,function(m,k,v){a[k] = v;});
    return a;
  }
  
  $.ajaxCORS = function (Field) 
  {
    var d = $.extend(Field, {
        type: "POST"
        ,dataType: "json"
        ,xhrFields: {
          withCredentials: true
        }
        ,beforeSend: function(xhr) {
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        }
        ,crossDomain: true
      });
    d.url = SERVER_ + d.url;
    
    return $.ajax(d);
  }
});